import os
import re


print("Please enter a system path:")
os.chdir(input())

files = [file for file in os.listdir() if os.path.isfile(file)]
files.sort()

videos = [file for file in files if file.endswith('.mkv')]
subs = [file for file in files if (file.endswith('.srt')
                                   or file.endswith('.ass'))]


serie_regex = re.compile(r"S\d{2}E\d{2}")
for file in videos:
    # loop through video files
    mo = serie_regex.search(file)
    if mo is not None:
        filename, file_extension = os.path.splitext(file)
        episode_id = mo.group(0)
        for sub in subs:
            # loop through subtitle files
            if episode_id in sub:
                sub_extension = os.path.splitext(sub)[1]
                os.rename(sub, filename + sub_extension)
                break
            else:
                continue
    else:
        # no regex match, log an error
        with open("_error.txt", "a", encoding="utf-8") as f:
            f.write(file + "\n")


print("Job done successfully.")
