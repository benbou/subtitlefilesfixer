This small script will prompt the user for a system path (full path).
Then it will look for .mkv video files and .srt (and/or .ass) subtitle files.
Then, for each video file, it will look for "S\d{2}E{2}" and look for a subtitle file that have the same match.
    If no match if found, log a warning.
Then it will take the video file name and apply it to the subtitle file (it will only keep the exetension).
